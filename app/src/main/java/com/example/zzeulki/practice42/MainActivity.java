package com.example.zzeulki.practice42;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final int GET_STRING = 1;
    TextView edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit = (TextView) findViewById(R.id.return_text);
    }

    public void Onclick(View v)
    {
        Intent i = new Intent(MainActivity.this,SubActivity.class);
        startActivityForResult(i,GET_STRING);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == GET_STRING)
            if(resultCode == RESULT_OK)
            {
                edit.setText(data.getStringExtra("INPUT_TEXT"));
            }
    }

}
