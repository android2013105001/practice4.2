package com.example.zzeulki.practice42;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class SubActivity extends AppCompatActivity {

    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        edit = (EditText) findViewById(R.id.editText);
    }

    public void Onclick(View v)
    {
        Intent i = new Intent();
        i.putExtra("INPUT_TEXT",edit.getText().toString());
        setResult(RESULT_OK,i);
        finish();
    }
}
